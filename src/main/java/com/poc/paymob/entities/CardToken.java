package com.poc.paymob.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "card_token")
public class CardToken
{
    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.IDENTITY) // identity will use the DB auto increment
    private int id;

    @Column(name = "paymob_token_id")
    private int paymobTokenId;

    @Column(name = "masked_pan")
    private String maskedPan;

    @Column(name = "token")
    private String token;

    @Column(name = "merchant_id")
    private int merchantId;

    @Column(name="card_subtype")
    private String cardSubType;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name="email")
    private String email;

    @Column(name = "paymob_order_id")
    private int paymobOrderid;

    @ManyToOne()
    @JoinColumn(name = "customer_id")
    @JsonIgnore
    private Customer customer;

    @Transient
    private int customerId;

    public CardToken() {
    }

    public CardToken(int paymobTokenId, String maskedPan, String token, int merchantId, String cardSubType, Date createdAt, String email, int paymobOrderid, Customer customer) {
        this.paymobTokenId = paymobTokenId;
        this.maskedPan = maskedPan;
        this.token = token;
        this.merchantId = merchantId;
        this.cardSubType = cardSubType;
        this.createdAt = createdAt;
        this.email = email;
        this.paymobOrderid = paymobOrderid;
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPaymobTokenId() {
        return paymobTokenId;
    }

    public void setPaymobTokenId(int paymobTokenId) {
        this.paymobTokenId = paymobTokenId;
    }

    public String getMaskedPan() {
        return maskedPan;
    }

    public void setMaskedPan(String maskedPan) {
        this.maskedPan = maskedPan;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(int merchantId) {
        this.merchantId = merchantId;
    }

    public String getCardSubType() {
        return cardSubType;
    }

    public void setCardSubType(String cardSubType) {
        this.cardSubType = cardSubType;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPaymobOrderid() {
        return paymobOrderid;
    }

    public void setPaymobOrderid(int paymobOrderid) {
        this.paymobOrderid = paymobOrderid;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getCustomerId() {
        return this.getCustomer().getId();
    }

    public void setCustomerId(int customerId) {
        if(this.getCustomer() == null)
            this.customer = new Customer();
        this.getCustomer().setId(customerId);
    }

    @Override
    public String toString() {
        return "CardToken{" +
                "id=" + id +
                ", paymobTokenId=" + paymobTokenId +
                ", maskedPan='" + maskedPan + '\'' +
                ", token='" + token + '\'' +
                ", merchantId=" + merchantId +
                ", cardSubType='" + cardSubType + '\'' +
                ", createdAt=" + createdAt +
                ", email='" + email + '\'' +
                ", paymobOrderid=" + paymobOrderid +
                ", customer=" + customer +
                ", customerId=" + customerId +
                '}';
    }
}
