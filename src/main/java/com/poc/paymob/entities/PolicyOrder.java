package com.poc.paymob.entities;


import javax.persistence.*;


@Entity
@Table(name = "policy_order")
public class PolicyOrder
{
    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.IDENTITY) // identity will use the DB auto increment
    private int id;

    // table relation
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @Column(name = "paymob_order_id")
    private int paymobOrderId;

    @Column(name = "amount_cents")
    private int amountCents;

    @Column(name = "expiration")
    private int expiration;

    @Column(name = "integration_id")
    private int integrationId;

    @Column(name = "lock_order_when_paid")
    private boolean lockOrderWhenPaid;

    @Column(name="currency")
    private String currency;

    @Transient
    private int customerId;

    public PolicyOrder() {
    }

    public PolicyOrder(Customer customer, int paymobOrderId, int amountCents, int expiration, int integrationId, boolean lockOrderWhenPaid, String currency) {
        this.customer = customer;
        this.paymobOrderId = paymobOrderId;
        this.amountCents = amountCents;
        this.expiration = expiration;
        this.integrationId = integrationId;
        this.lockOrderWhenPaid = lockOrderWhenPaid;
        this.currency = currency;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getPaymobOrderId() {
        return paymobOrderId;
    }

    public void setPaymobOrderId(int paymobOrderId) {
        this.paymobOrderId = paymobOrderId;
    }

    public int getAmountCents() {
        return amountCents;
    }

    public void setAmountCents(int amountCents) {
        this.amountCents = amountCents;
    }

    public int getExpiration() {
        return expiration;
    }

    public void setExpiration(int expiration) {
        this.expiration = expiration;
    }

    public int getIntegrationId() {
        return integrationId;
    }

    public void setIntegrationId(int integrationId) {
        this.integrationId = integrationId;
    }

    public boolean isLockOrderWhenPaid() {
        return lockOrderWhenPaid;
    }

    public void setLockOrderWhenPaid(boolean lockOrderWhenPaid) {
        this.lockOrderWhenPaid = lockOrderWhenPaid;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "PolicyOrder{" +
                "id=" + id +
                ", customer=" + customer +
                ", paymobOrderId=" + paymobOrderId +
                ", amountCents=" + amountCents +
                ", expiration=" + expiration +
                ", integrationId=" + integrationId +
                ", lockOrderWhenPaid=" + lockOrderWhenPaid +
                ", currency='" + currency + '\'' +
                '}';
    }
}
