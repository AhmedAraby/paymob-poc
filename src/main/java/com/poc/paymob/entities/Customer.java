package com.poc.paymob.entities;

import javax.persistence.*;


@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.IDENTITY) // identity will use the DB auto increment
    private int id;

    @Column(name = "name")
    private String name;

    public Customer() {
    }

    public Customer(String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
