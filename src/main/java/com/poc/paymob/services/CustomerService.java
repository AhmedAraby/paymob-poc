package com.poc.paymob.services;

import com.poc.paymob.models.paymob.PaymobCardToken;
import com.poc.paymob.dao.CardTokenDaoImpl;
import com.poc.paymob.dao.PolicyOrderDaoImpl;
import com.poc.paymob.entities.CardToken;
import com.poc.paymob.entities.Customer;
import com.poc.paymob.entities.PolicyOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService
{

    @Autowired
    private PolicyOrderDaoImpl policyOrderDao;

    @Autowired
    private CardTokenDaoImpl cardTokenDao;

    public void attach_cardToken_to_customer(PaymobCardToken paymobCardToken)
    {
        PolicyOrder policyOrder = policyOrderDao.getPolicyOrderBy_paymobOrderId(paymobCardToken.getOrder_id());
        Customer customer = policyOrder.getCustomer();
        CardToken cardToken = new CardToken(
                paymobCardToken.getId(), paymobCardToken.getMasked_pan(), paymobCardToken.getToken(),
                paymobCardToken.getMerchant_id(), paymobCardToken.getCard_subtype(),
                paymobCardToken.getCreated_at(), paymobCardToken.getEmail(), paymobCardToken.getOrder_id(), customer);
        cardTokenDao.save(cardToken);

        return ;
    }
}
