package com.poc.paymob.payment.services;

import com.poc.paymob.models.RecurrentPaymentRequestModel;
import com.poc.paymob.models.paymob.paymentToken.PaymobCustomerBillingData;
import com.poc.paymob.models.paymob.order.PaymobOrderResponse;
import com.poc.paymob.models.paymob.paymentToken.PaymobPaymentTokenResponse;
import com.poc.paymob.dao.CustomerDaoImpl;
import com.poc.paymob.dao.PolicyOrderDaoImpl;
import com.poc.paymob.entities.Customer;
import com.poc.paymob.entities.PolicyOrder;
import com.poc.paymob.models.paymob.recurrentPayment.PaymobRecurrentPaymentResponse;
import com.poc.paymob.payment.PaymobAPIs;
import com.poc.paymob.payment.PaymobHttp;
import com.poc.paymob.payment.PaymobIntegrations;
import com.poc.paymob.payment.PaymobTokens;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;

@Component
public class PaymobService {
    @Autowired
    PaymobHttp paymobHttp;

    @Autowired
    CustomerDaoImpl customerDao;

    @Autowired
    PolicyOrderDaoImpl policyOrderDao;

    public PaymobPaymentTokenResponse getPaymentKey_2steps(
            String integration_id,
            PolicyOrder policyOrder,
            PaymobCustomerBillingData customerBillingData)
    {
        PaymobOrderResponse paymobOrder = createOrder(policyOrder);

        PaymobPaymentTokenResponse paymentToken = paymobHttp.paymentKeyRequest(
                PaymobTokens.AUTH_TOKEN, PaymobAPIs.PAYMENT_KEY_API,
                String.valueOf(policyOrder.getAmountCents()), policyOrder.getExpiration(),
                paymobOrder.getId(), "EGP", integration_id,
                policyOrder.isLockOrderWhenPaid(), customerBillingData);

        return paymentToken;
    }

    // transactional work as proxy if create order is called locally
    // transactional wont work.
    @Transactional
    public PaymobOrderResponse createOrder(PolicyOrder policyOrder)
    {
        // attach order to customer
        // and save the order in the data base
        Customer customer = customerDao.get(policyOrder.getCustomerId());
        policyOrder.setCustomer(customer);
        policyOrderDao.save(policyOrder);

        // request order from paymob
        PaymobOrderResponse orderResponse = paymobHttp.orderRequest(
                PaymobTokens.AUTH_TOKEN, PaymobAPIs.ORDER_API,
                "false", String.valueOf(policyOrder.getAmountCents()), "EGP",
                String.valueOf(policyOrder.getId()), new ArrayList<>());

        // attach database order to paymob order
        policyOrder.setPaymobOrderId(Integer.valueOf(orderResponse.getId()));
        policyOrderDao.update(policyOrder);
        System.out.println("policy order is : " + policyOrder);
        return orderResponse;
    }

    public PaymobRecurrentPaymentResponse recurrentPayment(RecurrentPaymentRequestModel recurrentPaymentRequestModel)
    {

        PaymobPaymentTokenResponse paymentToken = this.getPaymentKey_2steps(
                PaymobIntegrations.recurrentPaymentIntegration,
                recurrentPaymentRequestModel.getPolicyOrder(),
                recurrentPaymentRequestModel.getPaymobCustomerBillingData());
        return paymobHttp.recurrentPaymentRequest(recurrentPaymentRequestModel.getCardToken(), paymentToken.getToken());
    }

}
