package com.poc.paymob.payment;

public class PaymobAPIs
{
    // Accept Service APIs
    public static final String AUTHENTICATION_API = "https://accept.paymob.com/api/auth/tokens";
    public static final String ORDER_API = "https://accept.paymob.com/api/ecommerce/orders";
    public static final String PAYMENT_KEY_API = "https://accept.paymob.com/api/acceptance/payment_keys";
    public static final String RECURRENT_PAYMENT_API = "https://accept.paymob.com/api/acceptance/payments/pay";

}
