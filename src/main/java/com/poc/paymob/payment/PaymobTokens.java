package com.poc.paymob.payment;

import com.poc.paymob.models.paymob.authenticationToken.PaymobAuthenticationTokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PaymobTokens {

    @Autowired
    private PaymobHttp paymobHttp;

    public static final String API_KEY
            = "ZXlKaGJHY2lPaUpJVXpVeE1pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SndjbTltYVd4bFgzQnJJam94TWpBME5qRXNJbU5zWVhOeklqb2lUV1Z" +
            "5WTJoaGJuUWlMQ0p1WVcxbElqb2lhVzVwZEdsaGJDSjkuSFBxblJhVHVZVGlOQ1o2QjNGb2hxV0xPdUlxaFdZUnVJRlVyX21rUkdvdnMxX2ZfW" +
            "kVwMWRXWjJqamlvV3FIeU9WWUZBalhyWHA3VnN1WHpFd1NGTHc=";

    // Auth Token related
    // expires in 1 hour.
    // needed for every api call to Accept paymob service.
    public static String AUTH_TOKEN = null;
    public static long authTokenLastUpdate;
    public static final long HOUR_IN_MS = 40 * 60 * 1000;  // actually it is 40 minutes, less than hour for safety
    public boolean is_Auth_token_expired()
    {
        if(AUTH_TOKEN == null)
            return true;
        if(System.currentTimeMillis() - authTokenLastUpdate >= HOUR_IN_MS)
            return true;
        return false;
    }
    public void refreshAuthToken()
    {
        authTokenLastUpdate = System.currentTimeMillis();
        PaymobAuthenticationTokenResponse authenticationToken = paymobHttp.AuthenticationRequest(API_KEY, PaymobAPIs.AUTHENTICATION_API);
        AUTH_TOKEN = authenticationToken.getToken();
    }
}
