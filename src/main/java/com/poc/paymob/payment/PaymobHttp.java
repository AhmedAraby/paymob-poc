package com.poc.paymob.payment;

import com.poc.paymob.models.paymob.PaymobSource;
import com.poc.paymob.models.paymob.authenticationToken.PaymobAutehnticationTokenPayload;
import com.poc.paymob.models.paymob.authenticationToken.PaymobAuthenticationTokenResponse;
import com.poc.paymob.models.paymob.order.PaymobOrderItem;
import com.poc.paymob.models.paymob.order.PaymobOrderPayload;
import com.poc.paymob.models.paymob.order.PaymobOrderResponse;
import com.poc.paymob.models.paymob.paymentToken.PaymobCustomerBillingData;
import com.poc.paymob.models.paymob.paymentToken.PaymobPaymentKeyPayload;
import com.poc.paymob.models.paymob.paymentToken.PaymobPaymentTokenResponse;
import com.poc.paymob.exceptions.PaymobGeneralException;
import com.poc.paymob.models.paymob.recurrentPayment.PaymobCardTokenPaymentPayLoad;
import com.poc.paymob.models.paymob.recurrentPayment.PaymobRecurrentPaymentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Component
public class PaymobHttp {

    @Autowired
    private RestTemplate restTemplate;

    public PaymobAuthenticationTokenResponse AuthenticationRequest(
            String API_KEY, String AUTHENTICATION_API)
    {
        // set headers
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        //build body
        PaymobAutehnticationTokenPayload paymobAutehnticationPayload = new PaymobAutehnticationTokenPayload(API_KEY);
        HttpEntity<PaymobAutehnticationTokenPayload> httpEntity = new HttpEntity<>(paymobAutehnticationPayload, httpHeaders);

        // send the request
        PaymobAuthenticationTokenResponse authToken = null;
        try {

            authToken = restTemplate.exchange(
                    AUTHENTICATION_API, HttpMethod.POST,
                    httpEntity, PaymobAuthenticationTokenResponse.class
            ).getBody();
        }
        catch(RestClientException exc){
            HttpClientErrorException detailedExc = (HttpClientErrorException) exc;
            int statusCode = detailedExc.getRawStatusCode();
            String message = detailedExc.getMessage();  // we need to get better message, based on the status code, response body will be very useful
            throw new PaymobGeneralException(message, statusCode);
        }
        return authToken;
    }


    public PaymobOrderResponse orderRequest(
            String AUTH_TOKEN, String ORDER_API, String delivery_needed,
            String amount_cents, String currency,
            String merchant_order_id, List<PaymobOrderItem> items)
    {
        // set headers
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        //build body
        PaymobOrderPayload paymobOrderPayload = new PaymobOrderPayload(
                AUTH_TOKEN, delivery_needed,
                amount_cents, currency,
                merchant_order_id, items);

        HttpEntity<PaymobOrderPayload> httpEntity = new HttpEntity<>(paymobOrderPayload, httpHeaders);

        PaymobOrderResponse response = null;
        try {
            response = restTemplate.exchange(
                    ORDER_API, HttpMethod.POST,
                    httpEntity, PaymobOrderResponse.class).getBody();
        }
        catch(RestClientException exc){
            HttpClientErrorException detailedExc = (HttpClientErrorException) exc;
            int statusCode = detailedExc.getRawStatusCode();
            String message = detailedExc.getMessage();  // we need to get better message, based on the status code, response body will be very useful
            throw new PaymobGeneralException(message, statusCode);
        }
        return response;
    }

    public PaymobPaymentTokenResponse paymentKeyRequest(
            String AUTH_TOKEN, String PAYMENT_KEY_API,
            String amount_cents, int expiration,
            String order_id, String currency, String integration_id,
            boolean lock_order_when_paid, PaymobCustomerBillingData customerBillingData)
    {
        // set headers
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        // build the body
        PaymobPaymentKeyPayload paymentTokenPayload =
                new PaymobPaymentKeyPayload(
                        AUTH_TOKEN, amount_cents, expiration, order_id,
                        currency, integration_id, customerBillingData, lock_order_when_paid);
        HttpEntity<PaymobPaymentKeyPayload> httpEntity = new HttpEntity<>(paymentTokenPayload, httpHeaders);

        // send request
        PaymobPaymentTokenResponse response = null;
        try {
            response = restTemplate.exchange(
                    PAYMENT_KEY_API,
                    HttpMethod.POST,
                    httpEntity,
                    PaymobPaymentTokenResponse.class).getBody();
        }
        catch(RestClientException exc)
        {
            HttpClientErrorException detailedExc = (HttpClientErrorException) exc;
            int statusCode = detailedExc.getRawStatusCode();
            String message = detailedExc.getMessage();  // we need to get better message, based on the status code, response body will be very useful
            throw new PaymobGeneralException(message, statusCode);
        }
        return response;
    }

    public PaymobRecurrentPaymentResponse recurrentPaymentRequest(String cardToken, String paymentToken)
    {
        // set headers
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        // build the body
        PaymobSource source = new PaymobSource(cardToken, "TOKEN");
        PaymobCardTokenPaymentPayLoad paymobCardTokenPaymentPayLoad = new PaymobCardTokenPaymentPayLoad(source, paymentToken);
        HttpEntity<PaymobCardTokenPaymentPayLoad> httpEntity = new HttpEntity<>(paymobCardTokenPaymentPayLoad, httpHeaders);
        PaymobRecurrentPaymentResponse response = null;
        try {
            response = restTemplate.exchange(
                    PaymobAPIs.RECURRENT_PAYMENT_API,
                    HttpMethod.POST,
                    httpEntity,
                    PaymobRecurrentPaymentResponse.class).getBody();
        }
        catch(RestClientException exc)
        {
            HttpClientErrorException detailedExc = (HttpClientErrorException) exc;
            int statusCode = detailedExc.getRawStatusCode();
            String message = detailedExc.getMessage();  // we need to get better message, based on the status code, response body will be very useful
            throw new PaymobGeneralException(message, statusCode);
        }
        return response;
    }
}
