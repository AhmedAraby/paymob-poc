package com.poc.paymob.aspects;

import com.poc.paymob.payment.PaymobTokens;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RefreshPaymobAuthToken {

    @Autowired
    private PaymobTokens paymobTokens;

    @Before("execution(* com.poc.paymob.controllers..*(..))")
    public void refreshPaymobAuthToken()
    {
        if (paymobTokens.is_Auth_token_expired()) {
            System.out.println("auth token expired");
            paymobTokens.refreshAuthToken();
        }
    }
}
