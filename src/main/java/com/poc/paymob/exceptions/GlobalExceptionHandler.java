package com.poc.paymob.exceptions;


import com.poc.paymob.models.paymob.PaymobGeneralErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<PaymobGeneralErrorResponse> handlePaymobGeneralException(PaymobGeneralException paymobGeneralException)
    {
        System.out.println("inside paymob general Exception handler");
        PaymobGeneralErrorResponse paymobErrorResponse = new PaymobGeneralErrorResponse(paymobGeneralException.getMessage(), paymobGeneralException.getStatusCode());
        return ResponseEntity.status(paymobGeneralException.getStatusCode()).body(paymobErrorResponse);
    }
}
