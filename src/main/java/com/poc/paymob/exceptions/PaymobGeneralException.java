package com.poc.paymob.exceptions;

public class PaymobGeneralException extends RuntimeException {
    private int statusCode;

    public PaymobGeneralException(String message, int statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public PaymobGeneralException(String message, Throwable cause, int statusCode) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
