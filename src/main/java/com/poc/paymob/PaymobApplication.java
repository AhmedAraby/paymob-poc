package com.poc.paymob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymobApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymobApplication.class, args);
	}

}
