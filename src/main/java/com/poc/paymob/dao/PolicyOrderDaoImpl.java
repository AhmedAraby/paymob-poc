package com.poc.paymob.dao;

import com.poc.paymob.entities.PolicyOrder;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;


@Repository
public class PolicyOrderDaoImpl
{
    @Autowired
    private EntityManager entityManager;



    public void save (PolicyOrder policyOrder){
        Session  session = entityManager.unwrap(Session.class);
        session.save(policyOrder);
        return ;
    }

    @Transactional
    public void update (PolicyOrder policyOrder){
        Session  session = entityManager.unwrap(Session.class);
        session.update(policyOrder);
        return ;
    }

    public PolicyOrder getPolicyOrderBy_paymobOrderId(int paymobOrderId)
    {
        String queryStr = " select po from PolicyOrder po " +
                            " where po.paymobOrderId=:paymobOrderId";
        Session  session = entityManager.unwrap(Session.class);
        Query<PolicyOrder> policyOrderQuery = session.createQuery(queryStr, PolicyOrder.class);
        policyOrderQuery.setParameter("paymobOrderId", paymobOrderId);
        PolicyOrder policyOrder = policyOrderQuery.getSingleResult();

        return policyOrder;
    }
}
