package com.poc.paymob.dao;

import com.poc.paymob.entities.Customer;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;


@Repository
public class CustomerDaoImpl
{
    @Autowired
    private EntityManager entityManager;

    public Customer get(int id)
    {
        Session session = entityManager.unwrap(Session.class);
        return session.get(Customer.class, id);
    }
}
