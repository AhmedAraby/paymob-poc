package com.poc.paymob.dao;

import com.poc.paymob.entities.CardToken;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class CardTokenDaoImpl
{

    @Autowired
    EntityManager entityManager;

    public void save(CardToken cardToken)
    {
        Session session = entityManager.unwrap(Session.class);
        session.save(cardToken);
    }


    public List<CardToken> getTokensByCustomerId(int customerId)
    {
        String queryStr = " select ct from CardToken as ct " +
                        " where ct.customer.id=:customerId " +
                        " order by ct.id ";
        Session session = entityManager.unwrap(Session.class);
        TypedQuery<CardToken> cardTokenQuery = session.createQuery(queryStr, CardToken.class);
        cardTokenQuery.setParameter("customerId", customerId);
        List<CardToken> tokens = cardTokenQuery.getResultList();

        return tokens;
    }
}
