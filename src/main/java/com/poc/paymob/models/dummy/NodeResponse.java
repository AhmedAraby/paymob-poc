package com.poc.paymob.models.dummy;

public class NodeResponse {

    private boolean success;
    private Data data;

    public NodeResponse() {
    }

    public NodeResponse(boolean success, Data data) {
        this.success = success;
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "NodeResponse{" +
                "success=" + success +
                ", data=" + data +
                '}';
    }
}
