package com.poc.paymob.models.paymob.recurrentPayment;

import com.poc.paymob.models.paymob.PaymobSource;

public class PaymobCardTokenPaymentPayLoad
{
    private PaymobSource source;
    private String payment_token;

    public PaymobCardTokenPaymentPayLoad() {
    }

    public PaymobCardTokenPaymentPayLoad(PaymobSource source, String payment_token) {
        this.source = source;
        this.payment_token = payment_token;
    }

    public PaymobSource getSource() {
        return source;
    }

    public void setSource(PaymobSource source) {
        this.source = source;
    }

    public String getPayment_token() {
        return payment_token;
    }

    public void setPayment_token(String payment_token) {
        this.payment_token = payment_token;
    }

    @Override
    public String toString() {
        return "PaymobCardTokenPaymentPayLoad{" +
                "source=" + source +
                ", payment_token='" + payment_token + '\'' +
                '}';
    }
}
