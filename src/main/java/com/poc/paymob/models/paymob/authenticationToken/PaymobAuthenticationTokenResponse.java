package com.poc.paymob.models.paymob.authenticationToken;

public class PaymobAuthenticationTokenResponse {

    private String token;

    public PaymobAuthenticationTokenResponse(String token) {
        this.token = token;
    }

    public PaymobAuthenticationTokenResponse() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "PaymobAuthenticationTokenResponse{" +
                "token='" + token + '\'' +
                '}';
    }
}
