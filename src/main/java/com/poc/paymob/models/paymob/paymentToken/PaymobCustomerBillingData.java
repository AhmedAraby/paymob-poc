package com.poc.paymob.models.paymob.paymentToken;

/**
 * all fields have to present in the http request body of paymentKey Request
 */

public class PaymobCustomerBillingData
{
    // theses fields can not be set to "NA"
    private String first_name;
    private String last_name;
    private String email;
    private String phone_number;


    // theses fields could be "NA" if they are not exist
    private String apartment="NA";
    private String floor="NA";
    private String street="NA";
    private String building="NA";
    private String shipping_method="NA";
    private String postal_code="NA";
    private String city="NA";
    private String country="NA";
    private String state="NA";

    public PaymobCustomerBillingData() {
    }

    public PaymobCustomerBillingData(String first_name, String last_name, String email, String phone_number) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.phone_number = phone_number;
    }

    public PaymobCustomerBillingData(
            String first_name, String last_name, String email, String phone_number,
            String apartment, String floor, String street, String building,
            String shipping_method, String postal_code, String city, String country, String state) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.phone_number = phone_number;
        this.apartment = apartment;
        this.floor = floor;
        this.street = street;
        this.building = building;
        this.shipping_method = shipping_method;
        this.postal_code = postal_code;
        this.city = city;
        this.country = country;
        this.state = state;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getShipping_method() {
        return shipping_method;
    }

    public void setShipping_method(String shipping_method) {
        this.shipping_method = shipping_method;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "PaymobCustomerBillingData{" +
                "first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", phone_number='" + phone_number + '\'' +
                ", apartment='" + apartment + '\'' +
                ", floor='" + floor + '\'' +
                ", street='" + street + '\'' +
                ", building='" + building + '\'' +
                ", shipping_method='" + shipping_method + '\'' +
                ", postal_code='" + postal_code + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
