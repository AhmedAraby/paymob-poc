package com.poc.paymob.models.paymob.recurrentPayment;

import java.util.Map;

public class PaymobRecurrentPaymentResponse
{
    private String type;
    private Map<String, Object> obj;

    public PaymobRecurrentPaymentResponse() {
    }

    public PaymobRecurrentPaymentResponse(String type, Map<String, Object> obj) {
        this.type = type;
        this.obj = obj;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Object> getObj() {
        return obj;
    }

    public void setObj(Map<String, Object> obj) {
        this.obj = obj;
    }

    @Override
    public String toString() {
        return "PaymobRecurrentPaymentResponse{" +
                "type='" + type + '\'' +
                ", obj=" + obj +
                '}';
    }
}
