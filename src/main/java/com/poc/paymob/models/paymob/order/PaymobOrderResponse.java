package com.poc.paymob.models.paymob.order;

import java.util.Date;

public class PaymobOrderResponse
{
    private String id; // id of the order in "Accept" (paymob service) database, we can use to do actions on this order latter.
    private Date created_at; // date and time togeather
    private String amount_cents;
    private String currency;
    private String merchant_order_id; // if for the order in our database

    public PaymobOrderResponse() {
    }

    public PaymobOrderResponse(String id, Date created_at, String amount_cents, String currency, String merchant_order_id) {
        this.id = id;
        this.created_at = created_at;
        this.amount_cents = amount_cents;
        this.currency = currency;
        this.merchant_order_id = merchant_order_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getAmount_cents() {
        return amount_cents;
    }

    public void setAmount_cents(String amount_cents) {
        this.amount_cents = amount_cents;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMerchant_order_id() {
        return merchant_order_id;
    }

    public void setMerchant_order_id(String merchant_order_id) {
        this.merchant_order_id = merchant_order_id;
    }
}
