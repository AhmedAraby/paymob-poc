package com.poc.paymob.models.paymob.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

public class PaymobTransactionCallbackInfo {

    private int id;
    private boolean pending;
    private int amount_cents;
    private boolean success;
    private boolean is_auth;
    private boolean is_capture;
    private boolean is_standalone_payment;
    private boolean is_refunded;
    private boolean is_3d_secure;
    private int integration_id;
    private int profile_id; // of the customer !!??
    private boolean has_parent_transaction;
    private boolean is_canceled;

    public PaymobTransactionCallbackInfo() {
    }

    public PaymobTransactionCallbackInfo(
            int id, boolean pending, int amount_cents, boolean success, boolean is_auth, boolean is_capture,
            boolean is_standalone_payment, boolean is_refunded, boolean is_3d_secure, int integration_id,
            int profile_id, boolean has_parent_transaction, boolean is_canceled) {
        this.id = id;
        this.pending = pending;
        this.amount_cents = amount_cents;
        this.success = success;
        this.is_auth = is_auth;
        this.is_capture = is_capture;
        this.is_standalone_payment = is_standalone_payment;
        this.is_refunded = is_refunded;
        this.is_3d_secure = is_3d_secure;
        this.integration_id = integration_id;
        this.profile_id = profile_id;
        this.has_parent_transaction = has_parent_transaction;
        this.is_canceled = is_canceled;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }

    public int getAmount_cents() {
        return amount_cents;
    }

    public void setAmount_cents(int amount_cents) {
        this.amount_cents = amount_cents;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isIs_auth() {
        return is_auth;
    }

    public void setIs_auth(boolean is_auth) {
        this.is_auth = is_auth;
    }

    public boolean isIs_capture() {
        return is_capture;
    }

    public void setIs_capture(boolean is_capture) {
        this.is_capture = is_capture;
    }

    public boolean isIs_standalone_payment() {
        return is_standalone_payment;
    }

    public void setIs_standalone_payment(boolean is_standalone_payment) {
        this.is_standalone_payment = is_standalone_payment;
    }

    public boolean isIs_refunded() {
        return is_refunded;
    }

    public void setIs_refunded(boolean is_refunded) {
        this.is_refunded = is_refunded;
    }

    public boolean isIs_3d_secure() {
        return is_3d_secure;
    }

    public void setIs_3d_secure(boolean is_3d_secure) {
        this.is_3d_secure = is_3d_secure;
    }

    public int getIntegration_id() {
        return integration_id;
    }

    public void setIntegration_id(int integration_id) {
        this.integration_id = integration_id;
    }

    public int getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(int profile_id) {
        this.profile_id = profile_id;
    }

    public boolean isHas_parent_transaction() {
        return has_parent_transaction;
    }

    public void setHas_parent_transaction(boolean has_parent_transaction) {
        this.has_parent_transaction = has_parent_transaction;
    }

    public boolean isIs_canceled() {
        return is_canceled;
    }

    public void setIs_canceled(boolean is_canceled) {
        this.is_canceled = is_canceled;
    }

    @Override
    public String toString() {
        return "PaymobTransactionCallbackInfo{" +
                "id=" + id +
                ", pending=" + pending +
                ", amount_cents=" + amount_cents +
                ", success=" + success +
                ", is_auth=" + is_auth +
                ", is_capture=" + is_capture +
                ", is_standalone_payment=" + is_standalone_payment +
                ", is_refunded=" + is_refunded +
                ", is_3d_secure=" + is_3d_secure +
                ", integration_id=" + integration_id +
                ", profile_id=" + profile_id +
                ", has_parent_transaction=" + has_parent_transaction +
                ", is_canceled=" + is_canceled +
                '}';
    }
}
