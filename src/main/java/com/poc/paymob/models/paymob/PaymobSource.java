package com.poc.paymob.models.paymob;

public class PaymobSource
{
    private String identifier;
    private String subtype;

    public PaymobSource() {
    }

    public PaymobSource(String identifier, String subtype) {
        this.identifier = identifier;
        this.subtype = subtype;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    @Override
    public String toString() {
        return "PaymobSource{" +
                "identifier='" + identifier + '\'' +
                ", subtype='" + subtype + '\'' +
                '}';
    }
}
