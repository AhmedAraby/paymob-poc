package com.poc.paymob.models.paymob.authenticationToken;

public class PaymobAutehnticationTokenPayload {

    private String api_key;

    public PaymobAutehnticationTokenPayload(String api_key) {
        this.api_key = api_key;
    }

    public PaymobAutehnticationTokenPayload() {
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    @Override
    public String toString() {
        return "PaymobAutehnticationTokenPayload{" +
                "api_key='" + api_key + '\'' +
                '}';
    }
}
