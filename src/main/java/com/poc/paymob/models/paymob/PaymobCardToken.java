package com.poc.paymob.models.paymob;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymobCardToken
{
    private int id;
    private String token;
    private String masked_pan;
    private String card_subtype;
    private Date created_at;
    private int order_id;
    private int merchant_id;
    private String email;


    public PaymobCardToken() {
    }

    public PaymobCardToken(String token, String masked_pan, String card_subtype, Date created_at, int order_id, int merchant_id, String email) {
        this.token = token;
        this.masked_pan = masked_pan;
        this.card_subtype = card_subtype;
        this.created_at = created_at;
        this.order_id = order_id;
        this.merchant_id = merchant_id;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMasked_pan() {
        return masked_pan;
    }

    public void setMasked_pan(String masked_pan) {
        this.masked_pan = masked_pan;
    }

    public String getCard_subtype() {
        return card_subtype;
    }

    public void setCard_subtype(String card_subtype) {
        this.card_subtype = card_subtype;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(int merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "PaymobCardToken{" +
                "id=" + id +
                ", token='" + token + '\'' +
                ", masked_pan='" + masked_pan + '\'' +
                ", card_subtype='" + card_subtype + '\'' +
                ", created_at=" + created_at +
                ", order_id=" + order_id +
                ", merchant_id=" + merchant_id +
                ", email='" + email + '\'' +
                '}';
    }
}
