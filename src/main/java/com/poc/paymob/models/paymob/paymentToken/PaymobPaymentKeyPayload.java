package com.poc.paymob.models.paymob.paymentToken;

/**
 * the result payment token is
 * attached to
 *  - customer
 *  - order id
 * have it's own
 *  - amount_cents
 *  - expiration.
 */

public class PaymobPaymentKeyPayload
{
    private String auth_token;
    private String amount_cents;  // could be different from the price we specified in the order request.
    private int expiration; // for the payment token in seconds
    private String order_id;
    private String currency;
    private String integration_id;  // represent the payment channel / method used.
    private PaymobCustomerBillingData billing_data;
    private boolean lock_order_when_paid; // try boolean then enum


    public PaymobPaymentKeyPayload(
            String auth_token, String amount_cents, int expiration, String order_id,
            String currency, String integration_id, PaymobCustomerBillingData billing_data,
            boolean lock_order_when_paid) {
        this.auth_token = auth_token;
        this.amount_cents = amount_cents;
        this.expiration = expiration;
        this.order_id = order_id;
        this.currency = currency;
        this.integration_id = integration_id;
        this.billing_data = billing_data;
        this.lock_order_when_paid = lock_order_when_paid;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public String getAmount_cents() {
        return amount_cents;
    }

    public void setAmount_cents(String amount_cents) {
        this.amount_cents = amount_cents;
    }

    public int getExpiration() {
        return expiration;
    }

    public void setExpiration(int expiration) {
        this.expiration = expiration;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getIntegration_id() {
        return integration_id;
    }

    public void setIntegration_id(String integration_id) {
        this.integration_id = integration_id;
    }

    public PaymobCustomerBillingData getBilling_data() {
        return billing_data;
    }

    public void setBilling_data(PaymobCustomerBillingData billing_data) {
        this.billing_data = billing_data;
    }

    public boolean getLock_order_when_paid() {
        return lock_order_when_paid;
    }

    public void setLock_order_when_paid(boolean lock_order_when_paid) {
        this.lock_order_when_paid = lock_order_when_paid;
    }

    @Override
    public String toString() {
        return "PaymobPaymentKeyPayload{" +
                "auth_token='" + auth_token + '\'' +
                ", amount_cents='" + amount_cents + '\'' +
                ", expiration=" + expiration +
                ", order_id='" + order_id + '\'' +
                ", currency='" + currency + '\'' +
                ", integration_id='" + integration_id + '\'' +
                ", billing_data=" + billing_data +
                ", lock_order_when_paid='" + lock_order_when_paid + '\'' +
                '}';
    }
}
