package com.poc.paymob.models.paymob.order;

import java.util.List;

public class PaymobOrderPayload
{
    private String auth_token;
    private String delivery_needed; // try boolean then make it enum if it did not work.
    private String amount_cents;
    private String currency;
    private String merchant_order_id; // id of the order in our data base
    private List<PaymobOrderItem> items ; // items/services we are going to sell in this order, empty array if not needed.

    public PaymobOrderPayload() {
    }

    public PaymobOrderPayload(
            String auth_token, String delivery_needed,
            String amount_cents, String currency,
            String merchant_order_id, List<PaymobOrderItem> items) {
        this.auth_token = auth_token;
        this.delivery_needed = delivery_needed;
        this.amount_cents = amount_cents;
        this.currency = currency;
        this.merchant_order_id = merchant_order_id;
        this.items = items;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public String getDelivery_needed() {
        return delivery_needed;
    }

    public void setDelivery_needed(String delivery_needed) {
        this.delivery_needed = delivery_needed;
    }

    public String getAmount_cents() {
        return amount_cents;
    }

    public void setAmount_cents(String amount_cents) {
        this.amount_cents = amount_cents;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMerchant_order_id() {
        return merchant_order_id;
    }

    public void setMerchant_order_id(String merchant_order_id) {
        this.merchant_order_id = merchant_order_id;
    }

    public List<PaymobOrderItem> getItems() {
        return items;
    }

    public void setItems(List<PaymobOrderItem> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "PaymobOrderPayload{" +
                "auth_token='" + auth_token + '\'' +
                ", delivery_needed='" + delivery_needed + '\'' +
                ", amount_cents='" + amount_cents + '\'' +
                ", merchant_order_id='" + merchant_order_id + '\'' +
                ", items=" + items +
                '}';
    }
}
