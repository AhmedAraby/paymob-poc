package com.poc.paymob.models.paymob.paymentToken;

public class PaymobPaymentTokenResponse
{
    private String token;

    public PaymobPaymentTokenResponse() {
    }

    public PaymobPaymentTokenResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "PaymobPaymentTokenResponse{" +
                "token='" + token + '\'' +
                '}';
    }
}
