package com.poc.paymob.models;

import com.poc.paymob.entities.PolicyOrder;
import com.poc.paymob.models.paymob.paymentToken.PaymobCustomerBillingData;

public class RecurrentPaymentRequestModel
{
    private String cardToken;
    private PaymobCustomerBillingData paymobCustomerBillingData;
    private PolicyOrder policyOrder;

    public RecurrentPaymentRequestModel() {
    }

    public RecurrentPaymentRequestModel(String cardToken, PaymobCustomerBillingData paymobCustomerBillingData, PolicyOrder policyOrder) {
        this.cardToken = cardToken;
        this.paymobCustomerBillingData = paymobCustomerBillingData;
        this.policyOrder = policyOrder;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public PaymobCustomerBillingData getPaymobCustomerBillingData() {
        return paymobCustomerBillingData;
    }

    public void setPaymobCustomerBillingData(PaymobCustomerBillingData paymobCustomerBillingData) {
        this.paymobCustomerBillingData = paymobCustomerBillingData;
    }

    public PolicyOrder getPolicyOrder() {
        return policyOrder;
    }

    public void setPolicyOrder(PolicyOrder policyOrder) {
        this.policyOrder = policyOrder;
    }

    @Override
    public String toString() {
        return "RecurrentPaymentRequestModel{" +
                "cardToken='" + cardToken + '\'' +
                ", paymobCustomerBillingData=" + paymobCustomerBillingData +
                ", policyOrder=" + policyOrder +
                '}';
    }
}
