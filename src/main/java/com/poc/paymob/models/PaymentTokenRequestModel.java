package com.poc.paymob.models;

import com.poc.paymob.entities.PolicyOrder;
import com.poc.paymob.models.paymob.paymentToken.PaymobCustomerBillingData;

public class PaymentTokenRequestModel
{
    private PaymobCustomerBillingData paymobCustomerBillingData;
    private PolicyOrder policyOrder;

    public PaymentTokenRequestModel() {
    }

    public PaymentTokenRequestModel(PaymobCustomerBillingData paymobCustomerBillingData, PolicyOrder policyOrder) {
        this.paymobCustomerBillingData = paymobCustomerBillingData;
        this.policyOrder = policyOrder;
    }

    public PaymobCustomerBillingData getPaymobCustomerBillingData() {
        return paymobCustomerBillingData;
    }

    public void setPaymobCustomerBillingData(PaymobCustomerBillingData paymobCustomerBillingData) {
        this.paymobCustomerBillingData = paymobCustomerBillingData;
    }

    public PolicyOrder getPolicyOrder() {
        return policyOrder;
    }

    public void setPolicyOrder(PolicyOrder policyOrder) {
        this.policyOrder = policyOrder;
    }

    @Override
    public String toString() {
        return "PaymentTokenRequestModel{" +
                "paymobCustomerBillingData=" + paymobCustomerBillingData +
                ", policyOrder=" + policyOrder +
                '}';
    }
}
