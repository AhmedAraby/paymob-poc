package com.poc.paymob.controllers;

import com.poc.paymob.dao.CardTokenDaoImpl;
import com.poc.paymob.entities.CardToken;
import com.poc.paymob.models.RecurrentPaymentRequestModel;
import com.poc.paymob.models.paymob.PaymobSource;
import com.poc.paymob.models.paymob.paymentToken.PaymobCustomerBillingData;
import com.poc.paymob.models.paymob.recurrentPayment.PaymobCardTokenPaymentPayLoad;
import com.poc.paymob.models.paymob.recurrentPayment.PaymobRecurrentPaymentResponse;
import com.poc.paymob.payment.services.PaymobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/tokens")
public class CardTokenController
{
    @Autowired
    private CardTokenDaoImpl cardTokenDao;

    @Autowired
    private PaymobService paymobService;

    @GetMapping("")
    @CrossOrigin(origins = {"http://7d99ceaf4835.ngrok.io", "http://localhost:4200"})
    public ResponseEntity<List<CardToken>> getTokens(@RequestParam(value = "customerId") int customerId)
    {
        System.out.println("customerId is : " + customerId);
        List<CardToken> cardTokenList = cardTokenDao.getTokensByCustomerId(customerId);
        return ResponseEntity.ok(cardTokenList);
    }

    @PostMapping("/pay")
    @CrossOrigin(origins = {"http://7d99ceaf4835.ngrok.io", "http://localhost:4200"})
    public ResponseEntity<PaymobRecurrentPaymentResponse> payWithSavedToken(@RequestBody RecurrentPaymentRequestModel recurrentPaymentRequestModel)
    {
        System.out.println("inside - paywith saved token action method");
        PaymobRecurrentPaymentResponse response = this.paymobService.recurrentPayment(recurrentPaymentRequestModel);
        return ResponseEntity.ok(response);
    }
}
