package com.poc.paymob.controllers.test;

import com.poc.paymob.dao.CustomerDaoImpl;
import com.poc.paymob.dao.PolicyOrderDaoImpl;
import com.poc.paymob.entities.Customer;
import com.poc.paymob.entities.PolicyOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EntitiesTestController
{
    @Autowired
    private PolicyOrderDaoImpl policyOrderDao;

    @Autowired
    private CustomerDaoImpl customerDao;

    @PostMapping("policyOrder")
    public void savePolicyOrder(@RequestBody PolicyOrder policyOrder)
    {
        Customer customer = customerDao.get(1);
        policyOrder.setCustomer(customer);
        policyOrderDao.save(policyOrder);
        return ;
    }

    @PutMapping("policyOrder")
    public void updatePolicyOrder(@RequestBody PolicyOrder policyOrder) {
        policyOrderDao.update(policyOrder);
    }
}
