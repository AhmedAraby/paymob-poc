package com.poc.paymob.controllers.test;


import com.poc.paymob.models.dummy.NodeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@RestController
public class TestController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/get/test")
    public ResponseEntity<NodeResponse> testGet(@RequestBody NodeResponse nodeResponse){

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> httpEntity =
                new HttpEntity<>("iam spring controllers ", httpHeaders);
        NodeResponse response = restTemplate.exchange(
                "http://localhost:8040/signin/client",
                HttpMethod.GET,
                httpEntity,
                NodeResponse.class
        ).getBody();

        System.out.println("node js response : " + response);
        System.out.println("request body nodeJs response "  + nodeResponse);
        return ResponseEntity.ok(nodeResponse);
    }
}
