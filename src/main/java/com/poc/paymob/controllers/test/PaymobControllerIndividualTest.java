package com.poc.paymob.controllers.test;


import com.poc.paymob.models.paymob.authenticationToken.PaymobAuthenticationTokenResponse;
import com.poc.paymob.models.paymob.order.PaymobOrderResponse;
import com.poc.paymob.models.paymob.paymentToken.PaymobCustomerBillingData;
import com.poc.paymob.models.paymob.paymentToken.PaymobPaymentTokenResponse;
import com.poc.paymob.payment.PaymobAPIs;
import com.poc.paymob.payment.PaymobHttp;
import com.poc.paymob.payment.PaymobTokens;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


/**
 * aim of this controller is
 * to test each endPoint individually
 */

@RestController
public class PaymobControllerIndividualTest {

    @Autowired
    private PaymobHttp paymobHttp;


    @GetMapping("/test/auth_token")
    public PaymobAuthenticationTokenResponse authTokenTest()
    {
        System.out.println("inside - authTokenTest Action method");
        PaymobAuthenticationTokenResponse authToken = paymobHttp.AuthenticationRequest(PaymobTokens.API_KEY,PaymobAPIs.AUTHENTICATION_API);
        return authToken;
    }


    @GetMapping("/test/order")
    public ResponseEntity<PaymobOrderResponse> orderTest()
    {
        // order do not expire
        System.out.println("inside - orderTest Action method");
        PaymobOrderResponse orderResponse = paymobHttp.orderRequest(
                PaymobTokens.AUTH_TOKEN,PaymobAPIs.ORDER_API,
                "false", "100", "EGP",
                String.valueOf(System.currentTimeMillis()), new ArrayList<>());
        return ResponseEntity.status(201).body(orderResponse);
    }


    @PostMapping("/test/payment_key")
    public ResponseEntity<PaymobPaymentTokenResponse> paymentKeyTest(
            @RequestParam("order_id") String order_id,
            @RequestParam("integration_id") String integration_id,
            @RequestBody PaymobCustomerBillingData customerBillingData)
    {
        System.out.println("inside - paymentKeyTest Action method");
        PaymobPaymentTokenResponse paymentToken = paymobHttp.paymentKeyRequest(
                PaymobTokens.AUTH_TOKEN,PaymobAPIs.PAYMENT_KEY_API, "100",
                7200, order_id, "EGP", integration_id,
                false, customerBillingData);
        return ResponseEntity.ok(paymentToken);
    }

}
