package com.poc.paymob.controllers;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.poc.paymob.models.paymob.PaymobCardToken;
import com.poc.paymob.models.paymob.transaction.PaymobTransactionCallbackInfo;
import com.poc.paymob.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class TransactionCallbackController
{

    @Autowired
    private CustomerService customerService;

    @PostMapping("/transaction/callback")
    public void readTransactionCallback(@RequestBody Map<String, Object> body) throws JsonProcessingException {

        System.out.println("inside - readTransactionCallback method");
        System.out.println("body is \n : " + body);
        String type = (String) body.get("type");
        ObjectMapper objectMapper = new ObjectMapper();
        if(type.equals("TOKEN")){
            PaymobCardToken paymobCardToken = objectMapper.convertValue(body.get("obj"), PaymobCardToken.class);
            System.out.println("card token model " + paymobCardToken);
            customerService.attach_cardToken_to_customer(paymobCardToken);
        }
        else if(type.equals("TRANSACTION")){
            PaymobTransactionCallbackInfo transactionCallbackInfoModel = objectMapper.convertValue(body.get("obj"), PaymobTransactionCallbackInfo.class);
            System.out.println("transaction callback info model : " + transactionCallbackInfoModel);
        }
        else
            System.out.println("--------------------- callback type will not be handled ------------------------------------");

        return ;
    }
}
