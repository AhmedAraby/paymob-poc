package com.poc.paymob.controllers;

import com.poc.paymob.models.PaymentTokenRequestModel;
import com.poc.paymob.models.paymob.order.PaymobOrderResponse;
import com.poc.paymob.models.paymob.paymentToken.PaymobCustomerBillingData;
import com.poc.paymob.models.paymob.paymentToken.PaymobPaymentTokenResponse;
import com.poc.paymob.entities.PolicyOrder;
import com.poc.paymob.payment.PaymobAPIs;
import com.poc.paymob.payment.PaymobHttp;
import com.poc.paymob.payment.PaymobIntegrations;
import com.poc.paymob.payment.services.PaymobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PaymobController
{
    @Autowired
    private PaymobService paymobService;

    @Autowired
    private PaymobHttp paymobHttp;


    @PostMapping("/paymob/payment_token")
    @CrossOrigin(origins = {"http://7d99ceaf4835.ngrok.io", "http://localhost:4200"})
    public ResponseEntity<PaymobPaymentTokenResponse> getPaymentToken(
            @RequestBody PaymentTokenRequestModel paymentTokenRequestModel)
    {
        System.out.println("inside getPaymentToken method \n");
        PaymobPaymentTokenResponse paymentToken =
                paymobService.getPaymentKey_2steps(
                        PaymobIntegrations.onlineCardIntegration,
                        paymentTokenRequestModel.getPolicyOrder(),
                        paymentTokenRequestModel.getPaymobCustomerBillingData());
        return ResponseEntity.ok(paymentToken);
    }
}
